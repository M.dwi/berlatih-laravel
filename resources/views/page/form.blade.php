@extends('layout.master')
@section('title')
    Halaman Form
@endsection
@section('content')

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/signup" method="POST">
        <label> First Name:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label> Last Name:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label> Gender</label><br><br>
        <input type="radio" name="gn"> Male <br>
        <input type="radio" name="gn"> Female <br>
        <input type="radio" name="gn"> Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nat">
            <option value="ina"> Indonesian </option>
            <option value="chn"> Chinese </option>
            <option value="arb"> Arabic </option>
            <option value="spn"> Spanish </option>
            <option value="kor"> Korean </option>
        </select> <br><br>
        <label> Language Spoken: </label><br><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other<br><br>
        <label> Bio: </label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="Submit" value="Sign Up">
    </form>
@endsection